const app = getApp();
Page({

  data: {
    state:1,
    fy: { page: 1, count: 0, end: 1 }//分页
  },

  onLoad: function (options) {

  },

  onReady: function () {

  },

  onShow: function () {
    if (app.reg()) {
      this.setData({ user: wx.getStorageSync('userinfo'), uid: parseInt(wx.getStorageSync('FXID')) });
      this.get_lists();
    }
  },
  get_lists:function(){
    if (!this.data.fy.end) return false;
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/card_follow_lists',
      data: { uid: this.data.uid, state: this.data.state, page: this.data.fy.page, total: this.data.fy.count },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.data.fy = d.data.mark;
          this.setData({ lists: this.data.lists ? this.data.lists.concat(d.data.data) : d.data.data });
        } else {
          this.data.fy.end = 0;
          this.setData({ lists: '', msg: d.data.message });
        }
        wx.hideLoading();
      }
    });
  },
  onTab:function(e){
    this.setData({state:e.detail.index});
    this.resetFy();
    this.get_lists();
  },
  onHide: function () {

  },

  onUnload: function () {

  },

  onPullDownRefresh: function () {

  },
  onReachBottom: function () {
    this.get_lists();
  },
  resetFy: function () {//重置分页
    this.data.fy = { page: 1, count: 0, end: 1 };
    this.data.lists = '';
  }
})