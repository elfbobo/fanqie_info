const app = getApp();//点赞和评论管理
Page({
  data: {
    fy: { page: 1, count: 0, end: 1 }//分页
  },
  onLoad: function (options) {
    var uid = parseInt(wx.getStorageSync('FXID'));
    if (uid) {
      this.setData({ uid: uid });
      this.get_lists();
    }
  },
  onReady: function () {

  },
  onShow: function () {

  },
  get_lists: function () {
    if (!this.data.fy.end) return false;
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/api/zancom',
      data: { uid: this.data.uid, page: this.data.fy.page, total: this.data.fy.count },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.data.fy = d.data.mark;
          this.setData({ lists: this.data.lists ? this.data.lists.concat(d.data.data) : d.data.data });
        } else {
          this.data.fy.end = 0;
          this.setData({ msg: d.data.message });
        }
        wx.hideLoading();
      }
    });
  },
  onDetail: function (e) {
    var id = e.currentTarget.id;
    wx.navigateTo({
      url: '/pages/index/detail/index?id=' + id + "&bottom=0",
    })
  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {
    this.get_lists();
  },
  resetFy: function () {//重置分页
    this.data.fy = { page: 1, count: 0, end: 1 };
    this.data.lists = '';
  }
})