<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 省市区
 * @author chaituan@126.com
 */
class City_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'city';
	}
}