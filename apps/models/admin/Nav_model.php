<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 首页导航
 * @author chaituan@126.com
 */
class Nav_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'nav';
	}
	
	function cache() {
		$items = $this->getItems();
		set_Cache('nav',$items);
	}
}