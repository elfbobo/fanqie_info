<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 广告管理
 * @author chaituan@126.com
 */
class Ad_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'ad';
	}
	
	function get_cache(){
		return get_Cache('ad_group');
	}
}