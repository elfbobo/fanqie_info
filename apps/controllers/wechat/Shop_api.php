<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Shop_api extends CI_Controller {
	
	function shop_cat(){
		if(is_ajax_request()){
			$this->load->model ('admin/ShopCat_model','do');
			$items = $this->do->getItems('','','sort');
			if($items){
				$items = addimg_url($items, 'icon');
				AjaxResult(1, '',$items);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function shop (){
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Shop_model'=>'do'));
			$item = $this->do->getItem(array('uid'=>$data['uid']));
			
			if($item){
				$item['header'] = base_url($item['header']);
				$item['thumb'] = addimg_url(explode(',', $item['thumb']));
				$item['shebei'] = array(
						array('name'=>'wifi','value'=>'WIFI','checked'=>$item['wifi']?true:false),
						array('name'=>'stopc','value'=>'停车位','checked'=>$item['stopc']?true:false),
						array('name'=>'alipay','value'=>'支付宝付款','checked'=>$item['alipay']?true:false),
						array('name'=>'wxpay','value'=>'微信付款','checked'=>$item['wxpay']?true:false),
				);
				AjaxResult(1, '',$item);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function shop_lists(){
		if(is_ajax_request()){
			$data = Posts();
			$where = '';$order = "id desc";
			if(isset($data['state'])){//tab 导航栏
				$cid = '';
				if(isset($data['cid'])){//单独导航栏使用
					$cid = "scid=".$data['cid'];
				}
				
				if($data['state']==0){//最新入住 默认
					$where = $cid;
				}else if($data['state']==1){//人气
					$where = $cid;
					$order = "hits desc";
				}else if($data['state']==2){//附近
					$scope = calcScope($data['la'], $data['lg'], 500000);////附近50公里范围
					$where = "( latitude between {$scope['minLat']} and {$scope['maxLat']} ) and ( longitude between {$scope['minLng']} and {$scope['maxLng']} ) ". ($cid?" and $cid":'');
				}else if($data['state']==3){//热帖
					$where = $cid;
					$order = "sc desc";
				}
			}			
			
			if(isset($data['search'])){//搜索
				$v = $data['v'];
				$where = "sname like '%$v%'";
				$order = "id desc";
			}
			$page = Posts('page','num');$limit = PAGESIZES;$total = Posts('total','num');
			$this->load->model(array('admin/Shop_model'=>'do'));
			$items = $this->do->getItems($where,'',$order,$page,$limit,$total,true);
			$pagemenu = $this->do->pagemenu;
			if($items){		
				if(isset($data['state'])&&$data['state']==1){//附近人排序
					foreach ($items as $v){
						$locaArr = calcDistance($data['la'], $data['lg'], $v['latitude'], $v['longitude']);
						$v['sort'] = $locaArr;
						$news_s[] = $v;
						$sort[] = $locaArr;
					}
					array_multisort($sort,SORT_ASC,$news_s);
					$items = $news_s;
				}	
				AjaxResult_page(addimg_url($items,'header'),$pagemenu);
			}else{
				AjaxResult_error('暂无数据!~~~~(>_<)~~~~');
			}
		}
	}
	
	function shop_detail(){
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Shop_model'=>'do','admin/ShopUv_model'=>'uv','admin/ShopSc_model'=>'sc'));
			$item = $this->do->getItem(array('id'=>$data['id']));
			if($item){
				$item['header'] = base_url($item['header']);				
				if($item['thumb']){
					$thumb = explode(',', $item['thumb']);
					$item['thumb'] = addimg_url($thumb);
				}
				//店铺访问量
				$this->do->updates(array('hits'=>"+=1"),array('id'=>$data['id']));
				//独立IP
				$uv = $this->uv->getItem(array('uid'=>$item['uid']));
				if($uv){// 
					$this->uv->updates(array('num'=>"+=1",'addtime'=>time()),array('uid'=>$data['uid']));
				}else{
					$this->uv->add(array('uid'=>$data['uid'],'sid'=>$data['id'],'num'=>1,'addtime'=>time()));
					$this->do->updates(array('uv'=>"+=1"),array('id'=>$data['id']));
				}
				$sc = $this->sc->getItem(array('uid'=>$data['uid'],'sid'=>$item['id']),''); 
				if($sc){//判断是否收藏了店铺
					$item['is_sc'] = 1;
				}else{
					$item['is_sc'] = 0;
				}
				AjaxResult(1, '',$item);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function shop_sc() {
			$data = Posts();
			
			$this->load->model(array('admin/ShopSc_model'=>'do','admin/shop_model'=>'shop'));
			if($data['query']=="del"){
				$result = $this->do->deletes(array('id'=>$data['id']));//被赞删除
				if($result){
					$this->shop->updates(array('sc'=>"-=1"),array('id'=>$data['id']));
				}
				is_AjaxResult($result);
			}else{
				$u1 = $data['uid'];$u2 = $data['o_uid'];
				$data_add = array(
						'uid'=>$u1,'sid'=>$data['sid'],'header'=>$data['header'],'sname'=>$data['sname'],
						'content'=>$data['content'],'addtime'=>time()
				);
				$result = $this->do->add($data_add);//添加被赞
				//更新名片关注数量
				if($result){
					$this->shop->updates(array('sc'=>"+=1"),array('uid'=>$u2));
				}
				is_AjaxResult($result);
			}
	}
	
	function shop_sc_lists() {//收藏列表
		if(is_ajax_request()){
			$data = Posts();	
			$where = '';
			$this->load->model(array('admin/ShopSc_model'=>'do'));			
			$page = Posts('page','num');$limit = PAGESIZES;$total = Posts('total','num');
			$items = $this->do->getItems($where,'','id desc',$page,$limit,$total,true);
			$pagemenu = $this->do->pagemenu;
			AjaxResult_page($items,$pagemenu);			
		}
	}
}
