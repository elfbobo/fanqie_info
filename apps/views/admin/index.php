<?php echo template('admin/header'); echo template('admin/sider');?>

<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<div class="panel_box row">
			<div class="col" style="margin-bottom: 10px">
				<blockquote class="layui-elem-quote title">亲爱的管理员，欢迎您使用番茄后台管理系统，您上次登录IP为：<?php echo $loginUser['last_login_ip'];?></blockquote>
			</div>
		</div>
		
		<div class="row">
			<div class="sysNotice col">
				<blockquote class="layui-elem-quote title">昨天会员走势图</blockquote>
				<div class="layui-elem-quote layui-quote-nm">
					<div id="main" style="width:100%;height:400px;"></div>
				</div>
			
				<blockquote class="layui-elem-quote title">系统基本参数</blockquote>
				
					<table class="layui-table">
						<colgroup>
							<col width="150">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<td>当前版本</td>
								<td class="version">V 3.0</td>
							</tr>
							<tr>
								<td>开发作者</td>
								<td class="author">chaituan@126.com</td>
							</tr>
							<tr>
								<td>网站首页</td>
								<td class="homePage">index.html</td>
							</tr>
							<tr>
								<td>最大上传限制</td>
								<td class="maxUpload">1M</td>
							</tr>
							<tr>
								<td>数据库版本</td>
								<td class="dataBase"><?php echo $sql_version;?></td>
							</tr>
							<tr>
								<td>服务器环境</td>
								<td class="server"><?php echo PHP_OS;?>;
			                      <?php echo strpos($_SERVER['SERVER_SOFTWARE'], 'PHP')===false ? $_SERVER['SERVER_SOFTWARE'].' ; PHP/'.phpversion() : $_SERVER['SERVER_SOFTWARE'];?>
			                      ;</td>
							</tr>
						</tbody>
					</table>
			</div>
		</div>
		
		
	</div>
</div>

<?php echo template('admin/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'echarts.common.min.js'?>" ></script>
<script type="text/javascript">
//基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementById('main'));

// 指定图表的配置项和数据
var option = {
	    tooltip: {trigger: 'axis',axisPointer: {lineStyle: {color: '#57617B'}}},
	    grid: {left: '0%',right: '4%',bottom: '3%',containLabel: true},
	    xAxis: [{name: '时间',type: 'category',boundaryGap: false,
	        data: ['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00',
	               '12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00']
	    }],
	    yAxis: [{type:'value',name: '数量'}],
	    series: [{
	        name: '会员',type: 'line',smooth: true,symbol: 'circle',symbolSize: 5,showSymbol: false,itemStyle: {normal: {color: '#516b91'}},
	        data:<?php echo $zst;?>
	    }]
	};

// 使用刚指定的配置项和数据显示图表。
myChart.setOption(option);
</script>
<?php echo template('admin/footer');?>