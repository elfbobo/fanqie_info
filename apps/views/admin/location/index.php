<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
					<div class="layui-input-inline">
						<input type="text"  id="ip"  placeholder="请输入坐标" class="layui-input" >
					</div>
			    	<?php echo admin_btn('', 'btn',"sub",'','查询官方市区名字')?>
			    	<a style="margin:0 10px;" href="http://lbs.qq.com/tool/getpoint/" target="_blank">查看坐标</a>
			    	<span id="wz" style="color: red"></span>					
		</blockquote>
		<table  id="common" lay-filter="common" ></table>
	</div>
</div>

<?php echo template('admin/script');?>
<script type="text/html" id="operation">
<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>

<script>
$(function(){
	$('.sub').click(function(){
		var ip = $('#ip').val();
		$.ajax({
			url:"/adminct/location/getl",
			type:'post',
			data:{ip,ip},
			dataType:'json',
			success:function(d){
				$('#wz').html(d.data);
			}
		});
	});
	
});
//执行渲染
layui.table.render({
	elem: '#common',
	id:'common',
	height: 'full-250',
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {field: 'id', title: 'ID', width: 80},
	       {field: 'lname', title: '地区名称',edit:'text'},
	       {field: 'right', title: '操作',toolbar: '#operation'}
	       ]],
	limit: 20,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
	}
});
layui.table.on('edit(common)', function(obj){
	var data = {id:obj.data.id},key = "data["+obj.field+"]";
	data[key] = obj.value;
	$.post('<?php echo site_url("$dr_url/edits")?>',data,function(d){layer.msg(d.message)},'json');
});
</script>
<?php echo template('admin/footer');?>