<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
			<div class="layui-inline">添加</div>
			<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">菜单分组</label>
				<div class="layui-input-block">
					<select name="data[groupkey]"  lay-filter='s1'>
                                <?php foreach ($menuGroups as $value){?>
                                <option value="<?php echo $value['tkey']?>" <?php if ($value['tkey']==$groupkey)echo 'selected';?>><?php echo $value['name'];?></option>
                                <?php }?>
                            </select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">上级菜单</label>
				<div class="layui-input-block">
					<select name="data[pid]"  id="pmenu-select" lay-filter='s2' >
						<option value="0">顶级菜单</option>
                                <?php if(isset($pid)){foreach ($menuData as $value){?>
                                <option value="<?php echo $value['id'];?>" <?php if ($value['id']==$pid)echo 'selected';?>><?php echo $value['name'];?></option>
                                <?php }}?>
                            </select>
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">菜单名称</label>
				<div class="layui-input-block">
					<input type="text" name="data[name]" class="layui-input" max-length="10" lay-verify='required'>
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">菜单URL</label>
				<div class="layui-input-block">
					<input type="text" name="data[url]" class="layui-input"  lay-verify='required'>
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">排序数字</label>
				<div class="layui-input-block">
					<input type="text" name="data[sort_num]" class="layui-input" lay-verify='required|number'>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">是否显示</label>
				<div class="layui-input-block">
					<input type="checkbox" name="data[ishow]" value="1" checked="checked" lay-skin="switch">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
				<?php echo admin_btn($add_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript">
$(function(){
	layui.form.on('select(s1)', function(data){
		  var key = data.value;
		    $('#pmenu-select').empty();
		    //加载一级分类
		    $.post('/adminct/menu/getTopMemnu', {groupkey:key}, function(res) {
		        if ( res.state == 'ok' ) {
		            var data = res.message;
		            $('#pmenu-select').append('<option value="0">顶级分类</option>');
		            for ( var i = 0; i < data.length; i++ ) {
		                $('#pmenu-select').append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
		            }
		        }else{
		        	$('#pmenu-select').append('<option value="0">顶级分类</option>');
			    }
		        layui.form.render('select');
		    }, 'json');
		    
	});     
});
</script>
<?php echo template('admin/footer');?>