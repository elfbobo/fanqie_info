<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline ">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
		</blockquote>
		<form class="layui-form" method="post">
			<table class="layui-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>分组名称</th>
							<th>添加时间</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
                        <?php if (empty($items)){ ?>
						<tr>
							<td class="empty-table-td"><?php echo $emptyRecord;?></td>
						</tr>
                        <?php }else{ foreach ($items as $value){ ?>
                        <tr>
							<td><?php echo $value['id'];?></td>
							<td><?php echo $value['catname'];?></td>
							<td><?php echo format_time($value['addtime']);?></td>
							<td>
								<div class="layui-btn-group">
									<?php echo admin_btn(site_url($dr_url.'/edit/id-'.$value['id']),'edit','layui-btn-xs');?>
									<?php echo admin_btn(site_url($dr_url.'/del/id-'.$value['id']),'del','layui-btn-xs f_del');?>
								</div>
							 
							 </td>
						</tr>
                            <?php if(!isset($value['children']))continue;foreach ($value['children'] as $v){?>
                            <tr>
								<td><?php echo $v['id'];?></td>
								<td><?php echo ' ├ '.$v['catname'];?></td>
								<td><?php echo format_time($v['addtime']);?></td>
								<td>
									  <?php echo admin_btn(site_url($dr_url.'/edit/id-'.$v['id']),'edit','layui-btn-xs');?>
									  <?php echo admin_btn(site_url($dr_url.'/del/id-'.$v['id']),'del','layui-btn-xs f_del');?>
								</td>
							</tr>
                            <?php if(!isset($v['children']))continue;foreach ($v['children'] as $b){//第三级?>
                            	<tr>
									<td><?php echo $b['id'];?></td>
									<td><?php echo ' ├├ '.$b['catname'];?></td>
									<td><?php echo format_time($b['addtime']);?></td>
									<td>
										<div class="layui-btn-group">
										  <?php echo admin_btn(site_url($dr_url.'/edit/id-'.$b['id']),'edit','layui-btn-xs');?>
										  <?php echo admin_btn(site_url($dr_url.'/del/id-'.$b['id']),'del','layui-btn-xs f_del');?>
										</div>
									</td>
								</tr>
                                <?php }}}}?>
					</tbody>
			</table>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>