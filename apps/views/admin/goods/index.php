<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
					<div class="layui-input-inline">
						<input type="text"  id="table-find-val" placeholder="请输入名称" class="layui-input" lay-verify='required'>
					</div>
				    <?php echo admin_btn('', 'find',"",'lay-filter="table-find"')?>
					</form>
				</div>
				<div class="layui-inline f-right">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
		</blockquote>
		<table  id="common" lay-filter="common" ></table>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/html" id="operation">
<?php echo admin_btn(site_url($dr_url.'/edit/id-{{d.id}}'),'edit','layui-btn-xs');?>
<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script type="text/html" id="sth">
<input type="checkbox" lay-text='上架|下架' lay-skin="switch" lay-filter='open' {{# if(d.state==1){ }} checked {{#  } }}   data-url="<?php echo site_url($dr_url.'/lock/id-{{d.id}}')?>" >
</script>
<script>
//执行渲染
layui.table.render({
	elem: '#common',
	id:'common',
	height: 'full-250',
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {field: 'sort', title: '排序', width: 80,edit:'text'},
	       {field: 'id', title: 'ID', width: 80},
	       {field: 'gname', title: '名称',edit:'text'},
	       {field: 'mprice', title: '市场价格',edit:'text'},
	       {field: 'price', title: '价格',edit:'text'},
	       {field: 'stock', title: '库存',edit:'text'},
	       {field: 'integral', title: '积分',edit:'text'},
	       {field: 'exchange', title: '兑换',edit:'text'},
	       {field: 'xg', title: '限购',edit:'text'},
	       {field: 'state', title: '状态',toolbar: '#sth',width: 90},
	       {field: 'addtime', title: '添加时间',toolbar:'<div>{{Time(d.addtime, "%y-%M-%d %h:%m")}}</div>'},
	       {field: 'right', title: '操作',toolbar: '#operation'}
	       ]],
	limit: 20,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
	}
});
layui.table.on('edit(common)', function(obj){
	var data = {id:obj.data.id},key = "data["+obj.field+"]";
	data[key] = obj.value;
	$.post('<?php echo site_url("$dr_url/edits")?>',data,function(d){layer.msg(d.message)},'json');
});
</script>
<?php echo template('admin/footer');?>